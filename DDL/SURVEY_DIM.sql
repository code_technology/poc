create table dw.SURVEY_DIM (
	SURVEY_DIM_ID		int not null identity(1,1),
	SURVEY_ID			int not null,	
	SURVEY_NAME			varchar(256) not null,
	ACTIVE_IND			boolean not null,
	PLACEHOLDER_IND		boolean not null,
	LAST_UPDATE_DT		datetime not null,
	PRIMARY KEY (SURVEY_DIM_ID)
);
