create table dw.QUESTION_ANSWER_DIM (
	QUESTION_ANSWER_DIM_ID		int not null identity(1,1),
	QUESTION_ANSWER_ID			int not null,	
	QUESTION_TEXT				text not null,
	QUESTION_TAG				varchar	(15) not null,
	QUESTION_GOOD_AT			decimal(10,3) not null,
	SCORING_IND					smallint not null,
	COMMENT						text not null,
	ANSWER_TEXT					text not null,
	LAST_UPDATE_DT				datetime not null,
	PRIMARY KEY (QUESTION_ANSWER_DIM_ID)
);
