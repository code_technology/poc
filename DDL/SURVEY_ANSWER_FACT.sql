create table dw.SURVEY_ANSWER_FACT (
	SURVEY_ANSWER_FACT_ID	int not null identity(1,1),
	SCHEDULE_ID				int NOT null,	
	DUE_DT					date NOT null,
	DUE_DATE_DIM_ID			int NOT null,
	ADMINISTRATION_ID		int	NOT null,
	PATIENT_DIM_ID			int NOT null,
	PROCEDURE_ID			int NOT null,
	SCHEDULE_TYPE_DIM_ID	int NOT null,
	SURVEY_DIM_ID			int NOT null,
	QUESTION_ANSWER_DIM_ID	int NOT null,
	SCORE_AMT				double NOT null,
	LAST_UPDATE_DT			datetime NOT null,
	PRIMARY KEY (SURVEY_ANSWER_FACT_ID)
);