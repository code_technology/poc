create table dw.LOCATION_DIM (
	LOCATION_DIM_ID		int not null identity(1,1),
	LOCATION_ID			int NOT null,	
	LOCATION_NAME		varchar	(128) NOT null,
	CITY				varchar	(128) NOT null,
	STATE_NAME			varchar	(128) NOT null,
	BRANDED_IND			BOOLEAN NOT null,
	PLACEHOLDER_IND		BOOLEAN NOT null,
	ACTIVE_IND			BOOLEAN NOT null,
	LAST_UPDATE_DT		datetime NOT null,
	PRIMARY KEY (LOCATION_DIM_ID)
);
