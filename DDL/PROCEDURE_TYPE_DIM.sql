create table dw.PROCEDURE_TYPE_DIM (
	PROCEDURE_TYPE_DIM_ID		int not null identity(1,1),
	PROCEDURE_TYPE_ID			int not null,	
	PROCEDURE_NAME				varchar	(50) not null,
	PROCEDURE_PART				varchar	(40) not null,
	PROCEDURE_DESCRIPTOR		varchar	(20) not null,
	PLACEHOLDER_IND				BOOLEAN not null,
	ACTIVE_IND					BOOLEAN not null,
	LAST_UPDATE_DT				datetime not null,
	PRIMARY KEY (PROCEDURE_TYPE_DIM_ID)
)

