create table dw.SCHEDULE_TYPE_DIM (
	SCHEDULE_TYPE_DIM_ID		int not null identity(1,1),
	SCHEDULE_TYPE_ID			int not null,	
	SCHEDULE_NAME				varchar(100) not null,
	PERIOD_NAME					varchar(50) not null,
	ACTIVE_IND					boolean not null,
	PLACEHOLDER_IND				boolean not null,
	LAST_UPDATE_DT				datetime not null,
	PRIMARY KEY (SCHEDULE_TYPE_DIM_ID)
);
