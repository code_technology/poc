create table dw.PATIENT_DIM (
	PATIENT_DIM_ID		int not null identity(1,1),
	PATIENT_ID			int not null,	
	CURRENT_AGE			int not null,
	GENDER				varchar(1) not null,
	RACE				varchar(50) not null,
	DEATH_DT			date not null,
	PATIENT_LANGUAGE	varchar(100) not null,
	LAST_UPDATE_DT		datetime not null,
	PRIMARY KEY (PATIENT_DIM_ID)
);