select 
	 pst.id as SCHEDULE_TYPE_ID
	,pst.name as SCHEDULE_NAME
	,pst.active as ACTIVE_IND
	,pst.placeholder as PLACEHOLDER_IND
    ,psp.name as PERIOD_NAME
from patient_schedule_type pst 
join patient_schedule_period psp on pst.period = psp.id
