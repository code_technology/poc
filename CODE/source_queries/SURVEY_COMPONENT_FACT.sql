select 
	 ps.id as SCHEDULE_ID
	,ps.DUE_DATE as DUE_DT
	,ps.administration as ADMINISTRATION_ID
	,ps.patient_id as PATIENT_DIM_ID
	,ps.proc_id as PROCEDURE_ID
	,ps.type_id as SCHEDULE_TYPE_DIM_ID
    ,pst.definition_id as SURVEY_DIM_ID
	,cs.id as COMPONENT_DIM_ID
	,cs.score as SCORE_AMT
    -- add min, max, goodIsHigher

from patient_schedule ps
join patient_schedule_type pst on ps.type_id = pst.id
join patient_schedule_period psp on pst.period = psp.id 
join completed_score cs on ps.completed_id = cs.survey_id
where ps.active = 1 
and ps.counting = 1
and cs.score <> -1234.000
