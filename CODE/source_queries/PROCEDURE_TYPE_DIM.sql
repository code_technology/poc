select 
	 id as PROCEDURE_TYPE_ID
	,part as PROCEDURE_PART
    ,descriptor as PROCEDURE_DESCRIPTOR
    ,active as ACTIVE_IND
    ,placeholder as PLACEHOLDER_IND
from proc_type