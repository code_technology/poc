select 
	 csa.surveyAnswerID as QUESTION_ANSWER_ID
	,cq.text as QUESTION_TEXT
	,cq.tag as QUESTION_TAG
	,cq.good_at as QUESTION_GOOD_AT
	,cq.scoring as SCORING_IND
	,csa.comment as COMMENT
	,ca.answerText as ANSWER_TEXT
from cb_surveyanswer csa 
join cb_answer ca on csa.answerid = ca.answerid
join cb_question cq on ca.questionid = cq.questionid
