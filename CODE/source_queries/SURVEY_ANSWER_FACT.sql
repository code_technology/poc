select 
	 ps.id as SCHEDULE_ID
	,ps.DUE_DATE as DUE_DT
	,ps.administration as ADMINISTRATION_ID
	,ps.patient_id as PATIENT_DIM_ID
	,ps.proc_id as PROCEDURE_ID
	,ps.type_id as SCHEDULE_TYPE_DIM_ID
    ,pst.definition_id as SURVEY_DIM_ID
    ,csa.surveyAnswerID as QUESTION_ANSWER_DIM
	,ca.score as SCORE_AMT
from patient_schedule ps
join patient_schedule_type pst on ps.type_id = pst.id
join patient_schedule_period psp on pst.period = psp.id
join cb_surveyanswer csa on ps.completed_id = csa.filledSurveyID
join cb_answer ca on csa.answerid = ca.answerid
join cb_question cq on ca.questionid = cq.questionid
where ps.active = 1 and ps.counting = 1
-- due date in the past or completed_id is not null