select 
	 loc.locationid as LOCATION_ID
	,loc.name as LOCATION_NAME
    ,loc.city as CITY
    ,st.name as STATE_NAME
    ,loc.branded as BRANDED_IND
    ,loc.placeholder as PLACEHOLDER_IND
    ,loc.active as ACTIVE_IND
from cb_location loc
inner join cb_states st on loc.stateid = st.stateid