select 
	 cs.id as COMPONENT_ID
    ,cs.active as SCORE_ACTIVE_IND
    ,ssc.active as COMPONENT_ACTIVE_IND
    ,ssc.name as COMPONENT_NAME
    ,ssc.category as COMPONENT_CATEGORY
from completed_score cs 
join survey_score_component ssc on cs.component = ssc.id