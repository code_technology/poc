select 
	 orgid as ORGANIZATION_ID
    ,org.name as ORGANIZATION_NAME
    ,tp.name as TYPE
    ,org.notes as NOTES
from cb_organization org
inner join cb_orgtype tp on org.orgtypeid = tp.orgtypeid