SELECT 
	 pr.id as PROVIDER_ID
    ,pr.npi as NPI
    ,pr.specialty as SPECIALTY
	,CASE
		WHEN pr.active = 0 THEN 0
		WHEN NOT EXISTS (SELECT 1 FROM cb_department WHERE cb_department.provider_id = pr.id AND cb_department.active = 1) THEN 0
		ELSE 1 
	END AS ACTIVE_IND
	,CASE
		WHEN EXISTS (SELECT 1 FROM cb_department WHERE cb_department.provider_id = pr.id AND cb_department.departed = 0) THEN 0
		ELSE 1 
	END AS DEPARTED_IND 
    ,cc.firstName as FIRST_NAME
    ,cc.lastName as LAST_NAME
FROM provider pr 
inner join cb_contact cc on pr.contact_id = cc.contactid
