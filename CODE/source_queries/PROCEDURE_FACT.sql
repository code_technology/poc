SELECT 
	prc.id as PROCEDURE_ID
    ,concat(
		coalesce(prc.name, prc_t.name, concat('PX ', prc.id))
        ,case when coalesce(prc.orientation, '') <> '' and prc.name is not null and prc_t.name is not null
			then concat(' (', prc.orientation, ')') 
            else ''
		end
	) as PROCEDURE_NAME
    ,prc.proc_date as PROCEDURE_DT
    ,prc.is_revision as REVISION_IND
	,pp.name as POPULATION_NAME
    ,prc.received as RECEIVED_DT
    ,loc.orgid as ORGANIZATION_DIM_ID
    ,prc.provider_id as PROVIDER_DIM_ID
    ,prc.patient_id as PATIENT_DIM_ID
    ,loc.locationid as LOCATION_DIM_ID
	,prc.type_id as PROCEDURE_TYPE_DIM_ID
    ,(SELECT proc_field_value.value FROM proc_field_value
	WHERE prc.id = proc_field_value.proc_id AND proc_field_value.definition_id = 4 LIMIT 1) as ZIP_CODE
	,(SELECT proc_field_options.value FROM proc_field_value
	LEFT JOIN proc_field_options ON proc_field_options.id = proc_field_value.field_option_id
	WHERE prc.id = proc_field_value.proc_id AND proc_field_value.definition_id = 13 LIMIT 1) as SMOKING_STATUS
    ,prc.*
FROM proc prc
JOIN proc_type  prc_t ON prc_t.id = prc.type_id
join proc_population pp on prc_t.population_id = pp.id
join cb_department cd on prc.department_id = cd.id_department
join cb_location loc on cd.id_location = loc.locationid
where prc.active = 1
and prc.canceled = 0
--  patient_schedule
-- due date in the past or completed_id is not null
