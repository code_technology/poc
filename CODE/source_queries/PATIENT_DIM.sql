select 
	 p.id as PATIENT_ID
	,TIMESTAMPDIFF(year, p.birth, now()) AS CURRENT_AGE
    ,p.gender as GENDER
    ,prt.name as RACE
    ,p.death as DEATH_DT
    ,pl.name as PATIENT_LANGUAGE
from patient p
inner join patient_race_type prt on p.race = prt.id
join patient_language pl on p.language_id = pl.id

